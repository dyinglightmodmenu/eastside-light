<!--Please use Emojicopy (https://www.emojicopy.com) to insert Emoji's.-->

# 🎰 About This Mod 💳

Modifies [Dying Light's](https://dyinglightgame.com/dying-light/) PVP to have a much more enjoyable experience

<details><!--Installation-->
<summary><h2>🖥️ Installation 💾</h2></summary>

- Open your Dying Light Directory

- Drag the mod into DW

- Then you're finished

#
</details><!--Installation-->

<details><!--Credits-->
<summary><h2>📃 Credits 💼</h2></summary>

- [Inari](https://gitlab.com/altyinari) (Creator of This Mod)

#